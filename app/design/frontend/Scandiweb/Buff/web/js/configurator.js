require([
    'jquery',
    'Magento_Ui/js/modal/modal'
], function ($, modal) {
    let texture = '';

    modal(
        {
            type: 'popup',
            responsive: true,
            innerScroll: true,
            title: 'Configurator',
            buttons: [{
                text: $.mage.__('Continue'),
                class: '',
                click: function () {
                    this.closeModal();
                }
            }]
        },
        $('#configurator-modal')
    );

    $('.configurator').on('click', function () {
        $('#configurator-modal').modal('openModal').css('display', 'flex');
    });

    $('.add-text').on('click', function () {
        const textWrapper = $('.dd-text-uploaded');
        textWrapper.val('').html($('.dd-text').val());
        textWrapper.draggable();
        textWrapper.removeClass('font-1 font-2 font-3').addClass($('.font-selector').val())
    })

    $('#texture-input').change(function(event) {
        readURL(this);
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            var filename = $('#texture-input').val();
            filename = filename.substring(filename.lastIndexOf('\\')+1);
            reader.onload = function(e) {
                texture = e.target.result;
                $('.texture-on-image').css('background-image', `url('${e.target.result}')`);
                $('.custom-file-label').text(filename);
            }
            reader.readAsDataURL(input.files[0]);
        }
        $('.alert').removeClass('loading').hide();
    }
});
