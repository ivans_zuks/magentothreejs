require([
    'jquery',
    'three',
    'OrbitControls',
    'GLTFLoader'
], function ($, THREE, OrbitControls, GLTFLoader) {
    var scene = new THREE.Scene();
    scene.background = new THREE.Color(0xffffff);
    var camera = new THREE.PerspectiveCamera(50, 500 / 400, 0.1, 1000);
    var renderer = new THREE.WebGLRenderer({canvas: shape});
    renderer.setSize(500, 500);
    camera.position.z = 10;
    const controls = new OrbitControls(camera, renderer.domElement);

    const textureLoader = new THREE.TextureLoader();
    const texture = textureLoader.load('media/model/texture.jpg');
    texture.encoding = THREE.sRGBEncoding;
    texture.flipY = false;

    var material = new THREE.MeshBasicMaterial( {
        map: texture,
        side: THREE.DoubleSide
    } );

    if (typeof GLTFLoader !== 'undefined') {
        console.log('***', 'GLTF Loaded');
        const gltfLoader = new GLTFLoader();

        gltfLoader.load(
            'media/model/buff_3d_high.gltf',
            (gltf) => {
                const model = gltf.scene;
                console.log('***', model);

                model.children[2].material = material;

                scene.add(model);

                // compute the box that contains all the stuff
                // from model and below
                const box = new THREE.Box3().setFromObject(model);

                const boxSize = box.getSize(new THREE.Vector3()).length();
                const boxCenter = box.getCenter(new THREE.Vector3());

                // set the camera to frame the box

                // update the Trackball controls to handle the new size
                controls.maxDistance = boxSize * 10;
                controls.target.copy(boxCenter);
                controls.update();
            });
    }

    var animate = function () {
        requestAnimationFrame(animate);

        renderer.render(scene, camera);
    };

    animate();
});
