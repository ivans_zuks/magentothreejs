define('three', ['js/source/three'], function (THREE) {
    window.THREE = THREE;
    return THREE;
});

define('OrbitControls', ['js/source/OrbitControls'], function () {
    return window.OrbitControls;
});

define('GLTFLoader', ['js/source/GLTFLoader'], function () {
    return window.GLTFLoader;
});

var config = {
    map: {
        '*': {
            'configurator': 'js/configurator',
            'threeDModel': 'js/3dmodel'
        }
    }
};
