'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');

sass.compiler = require('node-sass');

gulp.task('sass', function () {
    return gulp.src('./web/assets/scss/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./web/assets/css'));
});

gulp.task('sass:watch', function () {
    gulp.watch('./web/assets/scss/*.scss', ['sass']);
});
